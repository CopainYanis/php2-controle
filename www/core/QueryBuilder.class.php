<?php

namespace core;

class QueryBuilder {
    protected $connection;
    protected $query;
    protected $parameters;
    protected $alias;

    public function __construct()
    {

    }

    public function select(string $values = '*'): QueryBuilder {

    }

    public function from(string $table, string $alias): QueryBuilder {

    }

    public function where(string $conditions): QueryBuilder {

    }

    public function setParameter(string $key, string $value): QueryBuilder {

    }
//}